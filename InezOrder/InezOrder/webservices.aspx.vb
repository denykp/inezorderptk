﻿Imports System.IO
Imports System.Xml

Imports Microsoft.Web.Services3
Imports Microsoft.Web.Services3.Security
Imports Microsoft.Web.Services3.Security.Tokens


Public Class webservices


    Inherits System.Web.UI.Page
    Dim SqlDataSource1 As New SqlDataSource
    Public token_path As String = "C:/"
    Public userToken As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim gudang As String = ""
        Dim nip As String = ""

        'Declare
        SqlDataSource1.ID = "SqlDataSource1"
        Me.Page.Controls.Add(SqlDataSource1)
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("db_inezConnectionString").ConnectionString

        userToken = Request.Headers("Authorization")
        If userToken <> "" Then
            SqlDataSource1.SelectParameters.Clear()
            SqlDataSource1.SelectParameters.Add("token", userToken)
            SqlDataSource1.SelectCommand = "select gudang,nip from agolongansc where token = @token and token_expired >= getdate()"
            Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
            If dview.Count > 0 Then
                Dim dt As DataTable = dview.ToTable()
                For x As Integer = 0 To dview.Count - 1
                    gudang = dt.Rows(x)("gudang").ToString.Trim
                    nip = dt.Rows(x)("nip").ToString.Trim
                Next
            End If
        Else
            If Request.QueryString("type") <> "login" Then
                Response.Write("Token tidak ditemukan")
                Exit Sub
            End If
        End If

        '--> post pesanan
        '/webservices.aspx?type=post&nip_pemesan=xx&nama_pemesan=xx&kode_toko=xx&nama_toko=xx&kode_barang=xx&nama_barang=xx&qty=xx
        '--> get toko
        '/webservices.aspx?type=gettoko&nip=0117
        '--> get barang
        '/webservices.aspx?type=getbarang
        '--> login
        '/webservices.aspx?type=login&nip=0117&pass=1234
        'kode 000 = finish goods

        Select Case Request.QueryString("type")
            Case "get_last_order_detail"
                If validateToken(userToken) = False Then Exit Select
                get_last_order_detail()
            Case "get_last_order"
                If validateToken(userToken) = False Then Exit Select
                get_last_order(nip)
            Case "cek_sisa_stok"
                If validateToken(userToken) = False Then Exit Select
                If gudang = "" Then
                    MsgBox("Gudang belum diseting")
                    Exit Select
                End If
                cek_sisa_stok(gudang)
            Case "get_no_order"
                If validateToken(userToken) = False Then Exit Select
                get_no_order(nip)
            Case "gettoko"
                ' && @nip = "0117" ) Then
                If validateToken(userToken) = False Then Exit Select
                gettoko(nip)
            Case "getbarang"
                If validateToken(userToken) = False Then Exit Select
                If gudang = "" Then
                    MsgBox("Gudang belum diseting")
                    Exit Select
                End If
                getbarang(gudang)
            Case "get_stock_habis"
                If validateToken(userToken) = False Then Exit Select
                get_stock_habis(gudang)
            Case "getbanner"
                If validateToken(userToken) = False Then Exit Select
                getbanner()
        End Select

        If Request.RequestType = "POST" Then
            Select Case Request.QueryString("type")
                Case "login"
                    login(Request("nip"), Request("pass"))
                Case "change_password"
                    If validateToken(userToken) = False Then Exit Select
                    change_password()
                Case "order_post"
                    If validateToken(userToken) = False Then Exit Select
                    order_post()
            End Select
        End If
    End Sub
    Protected Sub ViewXML(ByVal sender As Object, ByVal e As EventArgs)
        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/xml"
        Response.WriteFile(Server.MapPath("~/Customers.xml"))
        Response.Flush()
        Response.[End]()
    End Sub

    Sub cek_sisa_stok(ByVal gudang As String)
        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("kode", Request("kd"))
        SqlDataSource1.SelectCommand = "select akhir1 from reports.dbo.astok" & gudang & " where kode = @kode"
        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        Dim dt As DataTable = dview.ToTable()
        Response.Write(dt.Rows(0)("akhir1").ToString.Trim())

    End Sub

    Sub get_stock_habis(ByVal gudang As String)
        SqlDataSource1.SelectParameters.Clear()
        'SqlDataSource1.SelectParameters.Add("kode", Request("kd"))
        SqlDataSource1.SelectCommand = "select kode,nama,akhir1 from astok" & gudang & " where akhir1 <= 0"
        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        If dview.Count > 0 Then
            Dim identity As XElement = New XElement("stock_kosong")
            Dim dt As DataTable = dview.ToTable()
            For x As Integer = 0 To dview.Count - 1
                Dim elm As XElement = New XElement("detail",
                New XElement("kode_barang", dt.Rows(x)("kode").ToString.Trim),
                New XElement("nama_barang", dt.Rows(x)("nama").ToString.Trim),
                New XElement("stock", dt.Rows(x)("akhir1").ToString.Trim)
                )
                identity.Add(elm)
            Next
            Dim xml As XElement = New XElement("xml", identity)
            Response.Clear()
            Response.Write(xml)
            Response.ContentType = "text/xml"
            Response.End()
        Else
            msgXML("Tidak ada stock kosong")
        End If

    End Sub

    Sub login(ByVal user As String, ByVal pass As String)
        Dim generateToken As UsernameToken
        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("user", user)
        SqlDataSource1.SelectParameters.Add("pass", pass)

        SqlDataSource1.SelectCommand = "select 'true' as status,nip,nama_spv,gudang from reports.dbo.[agolongansc]  where [nip] = @user AND [password] = @pass"
        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        Dim identity As XElement = New XElement("login")
        If dview.Count > 0 Then
            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes("x")
            'Dim waktu As String = Convert.ToString(Now)
            'Dim random_val As String = Convert.ToString(Rnd())
            'Dim md5 As md5tostring = New md5tostring

            generateToken = New UsernameToken(user, pass, PasswordOption.SendNone)
            saveToken(generateToken.Id, user)
            'Using imageFile = New FileStream(token_path + userToken.Id, FileMode.Create)
            '    imageFile.Write(byt, 0, byt.Length)
            '    imageFile.Flush()
            'End Using
            Dim dt As DataTable = dview.ToTable()
            Dim public_x As Integer
            For x As Integer = 0 To dview.Count - 1
                Dim elm As XElement = New XElement("detail",
                New XElement("status", "True"),
                New XElement("nip", dt.Rows(x)("nip").ToString.Trim),
                New XElement("nama_spv", dt.Rows(x)("nama_spv").ToString.Trim),
                New XElement("gudang", dt.Rows(x)("gudang").ToString.Trim),
                New XElement("token", generateToken.Id)
                )
                identity.Add(elm)
                public_x = x
            Next
            Dim xml As XElement = New XElement("xml", identity)
            Response.Clear()
            Response.Write(xml)
            Response.ContentType = "text/xml"
            Response.End()
            'Response.Write(xml.ToString)
        Else
            identity.Add(msgXML2("detail", "status:false", "username atau password invalid"))
            Dim xml As XElement = New XElement("xml", identity)
            Response.ContentType = "text/xml"
            Response.Write(xml)
        End If
        'Response.Write("test")

        'Session("nama_user") = dview.Table.Rows(0)("nama_user")
        'create file token
        'Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes("x")
        'Dim waktu As String = Convert.ToString(Now)
        'Dim random_val As String = Convert.ToString(Rnd())
        'Dim md5 As md5tostring = New md5tostring



        'Using imageFile = New FileStream(token_path + md5.execute(waktu + random_val), FileMode.Create)
        '    imageFile.Write(byt, 0, byt.Length)
        '    imageFile.Flush()
        'End Using

        'Response.Write("true-" + dview.Table.Rows(0)("nip").ToString.Trim + "-" + dview.Table.Rows(0)("nama_spv").ToString.Trim + "-" + md5.execute(waktu + random_val))
        'Else
        'Response.Write("false")
        'End If
    End Sub

    Sub get_no_order(ByVal nip As String)
        Dim no_order As String = nip + DateTime.Now.ToString("ddMMyyyyhhmmss")
        Response.Write(no_order)
    End Sub
    Sub order_post()
        Try
            Dim xstream = New MemoryStream()
            Request.InputStream.CopyTo(xstream)
            xstream.Position = 0
            Dim orderXML As XElement
            Dim order_response As XElement
            Dim total As IEnumerable(Of XElement)
            Dim detail As IEnumerable(Of XElement)
            orderXML = XElement.Load(xstream)

            total = orderXML.Elements("total")
            detail = orderXML.Element("detail").Elements("product")
            With SqlDataSource1
                'Dim no_order As String = Request("nip_pemesan") + DateTime.Now.ToString("ddMMyyyyhhmmss")
                .InsertParameters.Clear()
                .InsertParameters.Add("no_order", total.Elements("no_order").Value.Trim)
                .InsertParameters.Add("tanggal", Now)
                .InsertParameters.Add("nip_pemesan", total.Elements("nip").Value.Trim)
                .InsertParameters.Add("nama_pemesan", total.Elements("nama").Value.Trim)
                .InsertParameters.Add("kode_toko", total.Elements("kode_toko").Value.Trim)
                .InsertParameters.Add("nama_toko", total.Elements("nama_toko").Value.Trim)
                .InsertParameters.Add("gudang", total.Elements("gudang").Value.Trim)
                .InsertParameters.Add("total_harga", total.Elements("total_harga").Value.Trim)
                .InsertParameters.Add("diskon", total.Elements("diskon").Value.Trim)
                .InsertCommand = "insert into totorder_android ([no_order],[tanggal],[nip_pemesan],[nama_pemesan],[kode_toko],[nama_toko],[no_nota],[no_resi],[gudang],[total_harga],[diskon],[status]) values (@no_order,@tanggal,@nip_pemesan,@nama_pemesan,@kode_toko,@nama_toko,'','',@gudang,@total_harga,@diskon,0)"
                .Insert()
                For Each product As XElement In detail
                    .InsertParameters.Clear()
                    .InsertParameters.Add("no_order", total.Elements("no_order").Value.Trim)
                    .InsertParameters.Add("kode_barang", product.Element("kode_barang").Value.Trim)
                    .InsertParameters.Add("nama_barang", product.Element("nama_barang").Value.Trim)
                    .InsertParameters.Add("qty", product.Element("jumlah").Value.Trim)
                    .InsertParameters.Add("harga", product.Element("harga").Value.Trim)
                    .InsertParameters.Add("total_harga", CDec(product.Element("harga").Value.Trim) * CDec(product.Element("jumlah").Value.Trim))
                    .InsertCommand = "insert into detailorder_android ([no_order],[kode_barang],[nama_barang],[qty],[qty2],[harga],[total_harga]) values (@no_order,@kode_barang,@nama_barang,@qty,0,@harga,@total_harga)"
                    .Insert()
                Next

                .SelectParameters.Clear()
                .SelectParameters.Add("no_order", total.Elements("no_order").Value.Trim)
                .SelectCommand = "select * from totorder_android where no_order = @no_order"
                Dim dview As DataView = CType(.Select(DataSourceSelectArguments.Empty), DataView)
                If dview.Count > 0 Then
                    Dim dt As DataTable = dview.ToTable()

                    order_response = New XElement("xml", New XElement("order_post",
                                New XElement("no_order", dt.Rows(0)("no_order").ToString.Trim),
                                New XElement("tanggal", Format(dt.Rows(0)("tanggal"), "yyyy-MM-dd HH:mm:ss")),
                                New XElement("status", dt.Rows(0)("status").ToString.Trim)
                                ))
                    Response.Write(order_response)
                Else
                    msgXML("Nomer order tidak berhasil tersimpan")
                End If
            End With
        Catch ex As Exception
            msgXML(ex.Message)
        End Try


    End Sub

    Private Sub get_last_order_detail()
        SqlDataSource1.SelectParameters.Add("no_order", Request("no_order"))
        SqlDataSource1.SelectCommand = "select * from detailorder_android where no_order = @no_order"
        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)

        Dim identity As XElement = New XElement("last_order")
        If dview.Count > 0 Then
            Dim dt As DataTable = dview.ToTable()
            Dim public_x As Integer
            For x As Integer = 0 To dview.Count - 1

                Dim elm As XElement = New XElement("data",
                New XElement("kode_barang", dt.Rows(x)("kode_barang").ToString.Trim),
                New XElement("nama_barang", dt.Rows(x)("nama_barang").ToString.Trim),
                New XElement("qty", dt.Rows(x)("qty").ToString.Trim),
                New XElement("qty_terpenuhi", dt.Rows(x)("qty2").ToString.Trim),
                New XElement("harga", dt.Rows(x)("harga").ToString.Trim),
                New XElement("total_harga", dt.Rows(x)("total_harga").ToString.Trim)
                )

                identity.Add(elm)
                public_x = x
            Next
            Dim xml As XElement = New XElement("xml", identity)
            Response.Write(xml.ToString)
        Else
            msgXML("status:false")
        End If
    End Sub

    Private Sub get_last_order(ByVal nip As String)
        Try
            SqlDataSource1.SelectParameters.Add("nip", nip)
            SqlDataSource1.SelectParameters.Add("kode_toko", "%" & Request("kode_toko") & "%")
            SqlDataSource1.SelectCommand = "select distinct top 35 tanggal as tgl,no_order, CONVERT(VARCHAR(11),tanggal,106) as tanggal,kode_toko,nama_toko,status,no_nota,gudang,no_resi,total_harga from totorder_android where nip_pemesan = @nip " & IIf(IsNothing(Request("kode_toko")), "", "and kode_toko like @kode_toko") & " order by tgl desc"
            Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
            Dim identity As XElement = New XElement("last_order")
            If dview.Count > 0 Then
                Dim dt As DataTable = dview.ToTable()
                For x As Integer = 0 To dview.Count - 1
                    Dim elm As XElement = New XElement("data",
                    New XElement("no_order", dt.Rows(x)("no_order").ToString.Trim),
                    New XElement("no_nota", dt.Rows(x)("no_nota").ToString.Trim),
                    New XElement("no_resi", dt.Rows(x)("no_resi").ToString.Trim),
                    New XElement("tanggal", Format(dt.Rows(x)("tgl"), "yyyy-MM-dd HH:mm:ss")),
                    New XElement("kode_toko", dt.Rows(x)("kode_toko").ToString.Trim),
                    New XElement("nama_toko", dt.Rows(x)("nama_toko").ToString.Trim),
                    New XElement("gudang", dt.Rows(x)("gudang").ToString.Trim),
                    New XElement("total_harga", dt.Rows(x)("total_harga").ToString.Trim),
                    New XElement("status", dt.Rows(x)("status").ToString.Trim)
                    )
                    identity.Add(elm)
                Next
                Dim xml As XElement = New XElement("xml", identity)

                Response.Write(xml.ToString)
            Else
                msgXML("status:false")
            End If
        Catch ex As Exception
            msgXML(ex.Message)
        End Try
    End Sub

    Private Sub gettoko(ByVal nip As String)
        SqlDataSource1.SelectParameters.Add("nip", nip)
        SqlDataSource1.SelectCommand = "select ag.nip,ag.nama_spv, ac.kode as kode_toko, ac.nama as nama_toko, ac.disc as disc,ac.alamat as alamat from acustomer ac JOIN agolongansc ag ON ac.wilayah = ag.kode  where (ac.tutup <> '1' or ac.tutup is null)  and ag.nip = @nip"

        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)

        Dim identity As XElement = New XElement("toko")
        If dview.Count > 0 Then
            Dim dt As DataTable = dview.ToTable()
            Dim public_x As Integer
            For x As Integer = 0 To dview.Count - 1
                Dim elm As XElement = New XElement("detail",
                New XElement("nip", dt.Rows(x)("nip").ToString.Trim),
                New XElement("nama_spv", dt.Rows(x)("nama_spv").ToString.Trim),
                New XElement("kode_toko", dt.Rows(x)("kode_toko").ToString.Trim),
                New XElement("nama_toko", dt.Rows(x)("nama_toko").ToString.Trim),
                New XElement("disc", dt.Rows(x)("disc").ToString.Trim),
                New XElement("alamat", dt.Rows(x)("alamat").ToString.Trim)
                )
                identity.Add(elm)
                public_x = x
            Next
            Dim xml As XElement = New XElement("xml", identity)
            Response.Clear()
            Response.Write(xml)
            Response.ContentType = "text/xml"
            Response.End()
            'Response.Write(xml)
        Else
            msgXML("status:false")
        End If
    End Sub

    Private Sub getbarang(ByVal gudang As String)
        '000 = finish goods
        SqlDataSource1.SelectParameters.Add("kode_barang", "%" + Request("kd") + "%")

        SqlDataSource1.SelectCommand = "select a.kode, a.nama, b.keterangan as jenis, b.koleksi, a.hj1, akhir1 from astok" & gudang & " a  inner join ajenis b on a.jenis = b.kode where a.golongan = '000' and a.akhir1 > 0  and (a.kode like @kode_barang or a.nama like @kode_barang) order by a.kode asc"
        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)

        Dim identity As XElement = New XElement("barang")
        If dview.Count > 0 Then
            Dim dt As DataTable = dview.ToTable()
            Dim public_x As Integer
            For x As Integer = 0 To dview.Count - 1
                Dim elm As XElement = New XElement("detail",
                New XElement("kode", dt.Rows(x)("kode").ToString.Trim),
                New XElement("nama", dt.Rows(x)("nama").ToString.Trim),
                New XElement("harga", dt.Rows(x)("hj1").ToString.Trim),
                New XElement("jenis", dt.Rows(x)("jenis").ToString.Trim),
                New XElement("koleksi", dt.Rows(x)("koleksi").ToString.Trim),
                New XElement("gudang", gudang),
                New XElement("stock", dt.Rows(x)("akhir1").ToString.Trim)
                )
                identity.Add(elm)
                public_x = x
            Next
            Dim xml As XElement = New XElement("xml", identity)
            Response.Clear()
            Response.Write(xml)
            Response.ContentType = "text/xml"
            Response.End()
            'Response.Write(xml.ToString)
        Else
            msgXML("status:false")
        End If
    End Sub

    Private Sub change_password()
        With SqlDataSource1
            .SelectParameters.Clear()
            .SelectParameters.Add("token", userToken)
            .SelectCommand = "select password from agolongansc where token = @token"
            Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
            If dview.Count > 0 Then
                Dim dt As DataTable = dview.ToTable()
                If dt.Rows(0)("password").ToString.Trim = Request("pass_current") Then
                    .UpdateParameters.Clear()
                    .UpdateParameters.Add("pass_new", Request("pass_new"))
                    .UpdateParameters.Add("token", userToken)
                    .UpdateCommand = "update agolongansc set password = @pass_new where token = @token"
                    .Update()
                    msgXML("Password Berhasil Diganti")
                Else
                    msgXML("Password Invalid")
                End If
            End If
        End With
    End Sub

    Private Sub saveToken(ByVal token As String, ByVal nip As String)
        Try
            SqlDataSource1.UpdateParameters.Clear()
            SqlDataSource1.UpdateParameters.Add("token", token)
            SqlDataSource1.UpdateParameters.Add("token_expired", DateAdd("yyyy", 1, Now))
            SqlDataSource1.UpdateParameters.Add("nip", nip)
            SqlDataSource1.UpdateCommand = "update agolongansc set token = @token, token_expired = @token_expired where nip = @nip"
            SqlDataSource1.Update()
        Catch ex As Exception
            msgXML(ex.Message)
        End Try
    End Sub

    Private Function validateToken(ByVal token As String) As Boolean
        validateToken = False
        Try
            SqlDataSource1.SelectParameters.Clear()
            SqlDataSource1.SelectParameters.Add("token", token)
            SqlDataSource1.SelectCommand = "select nip from agolongansc where token = @token and token_expired >= getdate()"
            Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
            If dview.Count > 0 Then
                validateToken = True
            Else
                msgXML("Token Invalid")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function

    Private Sub msgXML(ByVal message As String, Optional message2 As String = "")
        Dim messageXML As XElement
        Dim message_tag As XName = XName.Get(IIf(message.Contains("status:"), "status", "message"))
        messageXML = New XElement("xml")
        messageXML.Add(New XElement(message_tag, Replace(message, "status:", "")))
        If message2 <> "" Then messageXML.Add(New XElement("message", message2))
        Response.ContentType = "text/xml"
        Response.Write(messageXML)
    End Sub

    Private Function msgXML2(ByVal tag As String, ByVal message As String, Optional message2 As String = "") As XElement
        Dim message_tag As XName = XName.Get(IIf(message.Contains("status:"), "status", "message"))
        msgXML2 = New XElement(XName.Get(tag))
        msgXML2.Add(New XElement(message_tag, Replace(message, "status:", "")))
        If message2 <> "" Then msgXML2.Add(New XElement("message", message2))
    End Function

    Private Sub getbanner()
        Dim url As String = Request.Url.AbsoluteUri
        url = url.Substring(0, InStr(url, Request.Url.AbsolutePath))
        Dim filePaths() As String = Directory.GetFiles(Server.MapPath("~/banner/"))
        Dim identity As XElement = New XElement("getbanner")
        For Each filePath As String In filePaths
            identity.Add(New XElement("banner", url & "inezorder/banner/" & Path.GetFileName(filePath)))
        Next
        Dim xml As XElement = New XElement("xml", identity)
        Response.Write(xml)
    End Sub
End Class