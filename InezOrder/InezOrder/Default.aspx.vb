﻿Public Class _Default
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("nama_user") = "" Then
            Response.Redirect("Login.aspx")
        End If
        
        If Request.QueryString("type") = "showdetail" Then
            Session("no_order") = Request("no_order")
            Label1.Text = "<script type='text/javascript'>callmodal();</script>"
        End If


        If Request.QueryString("type") = "layani" Then
            SqlDataSource1.UpdateParameters.Clear()
            SqlDataSource1.UpdateParameters.Add("no_order", Request("no_order"))
            SqlDataSource1.UpdateCommand = "update [main_android_orderPTK] set status = 1 where no_order = @no_order"
            SqlDataSource1.Update()
            Response.Redirect("Default.aspx")

        End If

        If Request.QueryString("type") = "batal" Then
            SqlDataSource1.UpdateParameters.Clear()
            SqlDataSource1.UpdateParameters.Add("no_order", Request("no_order"))
            SqlDataSource1.UpdateCommand = "update [main_android_orderPTK] set status = 0 where no_order = @no_order"
            SqlDataSource1.Update()
            Response.Redirect("Default.aspx")
        End If

        
        dt.Columns.Add("NO")
        dt.Columns.Add("TANGGAL")
        dt.Columns.Add("NIP PEMESAN")
        dt.Columns.Add("NAMA PEMESAN")
        dt.Columns.Add("KODE TOKO")
        dt.Columns.Add("NAMA TOKO")
        dt.Columns.Add("NO ORDER")
        dt.Columns.Add("NO NOTA")
        'dt.Columns.Add("KODE BARANG")
        'dt.Columns.Add("NAMA BARANG")
        'dt.Columns.Add("QTY")
        dt.Columns.Add("STATUS")
        dt.Columns.Add("TAMPIL DETAIL")
        dt.Columns.Add("EKSEKUSI")


        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        Dim rowcount As Integer = dview.Table.Rows.Count - 1
        For A As Integer = 0 To dview.Table.Rows.Count - 1
            Dim R As DataRow = dt.NewRow


            R("NO") = A + 1
            R("TANGGAL") = Convert.ToString(dview.Table.Rows(A)("tanggal"))
            R("NIP PEMESAN") = Convert.ToString(dview.Table.Rows(A)("nip_pemesan"))
            R("NAMA PEMESAN") = Convert.ToString(dview.Table.Rows(A)("nama_pemesan"))
            R("KODE TOKO") = Convert.ToString(dview.Table.Rows(A)("kode_toko"))
            R("NAMA TOKO") = Convert.ToString(dview.Table.Rows(A)("nama_toko"))
            R("NO ORDER") = Convert.ToString(dview.Table.Rows(A)("no_order"))
            R("NO NOTA") = Convert.ToString(dview.Table.Rows(A)("no_nota"))
            'R("KODE BARANG") = Convert.ToString(dview.Table.Rows(A)("kode_barang"))
            'R("NAMA BARANG") = Convert.ToString(dview.Table.Rows(A)("nama_barang"))
            'R("QTY") = Convert.ToString(dview.Table.Rows(A)("qty"))
            Dim stts As String = Convert.ToString(dview.Table.Rows(A)("status"))
            If stts = "1" Then
                stts = "dilayani"
            Else
                stts = "belum dilayani"
            End If

            R("STATUS") = stts


            Dim no_order As String = Convert.ToString(dview.Table.Rows(A)("no_order"))


            'R("TAMPIL DETAIL") = "<input type='button' value='  GO  ' onclick='javascript:callmodal();'></input>"
            R("TAMPIL DETAIL") = "<input type='button' value='  GO  ' onclick='window.location.href=""default.aspx?status=" + Session("status") + "&type=showdetail&no_order=" + no_order + """ '></input>"



            
            If Convert.ToString(dview.Table.Rows(A)("status")) = "1" Then
                R("EKSEKUSI") = "<input type='button' value='Batal' onclick='window.location.href=""default.aspx?type=batal&no_order=" + no_order + """'></input>"
            Else
                R("EKSEKUSI") = "<input type='button' value='Layani' onclick='window.location.href=""default.aspx?type=layani&no_order=" + no_order + """'></input>"
            End If




            dt.Rows.Add(R)
            yourGrid.DataSource = dt

        Next
        yourGrid.DataBind()




        Try
            dt.DefaultView.RowFilter = Session("last_cari_non_tanggal_kolom") + " like '%" + Session("last_cari_non_tanggal") + "%'"
            yourGrid.DataSource = dt.DefaultView
            yourGrid.DataBind()

        Catch ex As Exception
        End Try



        Try

            For B As Integer = 0 To yourGrid.Items.Count - 1
                If yourGrid.Items(B).Cells.Item(8).Text = "dilayani" Then
                    yourGrid.Items(B).BackColor = Drawing.Color.Green
                Else
                    yourGrid.Items(B).BackColor = Drawing.Color.White
                End If
            Next

        Catch ex As Exception

        End Try




        If Request.QueryString("status") = "belum_dilayani" Then
            Try
                dt.DefaultView.RowFilter = "STATUS like '%belum%'"
                yourGrid.DataSource = dt.DefaultView
                yourGrid.DataBind()
                txtDate.Text = ""

                For B As Integer = 0 To yourGrid.Items.Count - 1
                    If yourGrid.Items(B).Cells.Item(8).Text = "dilayani" Then
                        yourGrid.Items(B).BackColor = Drawing.Color.Green
                    Else
                        yourGrid.Items(B).BackColor = Drawing.Color.White
                    End If
                Next
            Catch ex As Exception
                'lbl_warning.Visible = True
                'Response.Write(ex.ToString)
            End Try

        End If


        If Request.QueryString("status") = "dilayani" Then
            Try
                dt.DefaultView.RowFilter = "STATUS = 'dilayani'"
                yourGrid.DataSource = dt.DefaultView
                yourGrid.DataBind()
                txtDate.Text = ""


                For B As Integer = 0 To yourGrid.Items.Count - 1
                    If yourGrid.Items(B).Cells.Item(8).Text = "dilayani" Then
                        yourGrid.Items(B).BackColor = Drawing.Color.Green
                    Else
                        yourGrid.Items(B).BackColor = Drawing.Color.White
                    End If
                Next

            Catch ex As Exception
                'lbl_warning.Visible = True
                'Response.Write(ex.ToString)
            End Try
        End If


        'you


    End Sub

   
    Protected Sub MainGrid_PageIndexChanged(ByVal source As Object, ByVal e As DataGridPageChangedEventArgs) Handles yourGrid.PageIndexChanged
        yourGrid.CurrentPageIndex = e.NewPageIndex
        yourGrid.DataBind() 'rebinds the grid

        If Session("cari") = "tanggal" Then
            dt.DefaultView.RowFilter = "[TANGGAL CHECK IN] like '%" + txtDate.Text + "%'"
            yourGrid.DataSource = dt.DefaultView
            yourGrid.DataBind()
        ElseIf Session("cari") = "non_tanggal" Then
            dt.DefaultView.RowFilter = DropDownList1.SelectedItem.Value + " like '%" + TextBox1.Text + "%'"
            yourGrid.DataSource = dt.DefaultView
            yourGrid.DataBind()
        End If

    End Sub

    Protected Sub btn_reset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_reset.Click
        Session("last_cari_non_tanggal") = ""
        Session("last_cari_non_tanggal_kolom") = ""
        Session("status") = ""
        Response.Redirect("default.aspx")
    End Sub

    Protected Sub btn_cari_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_cari.Click

        Session("cari") = "non_tanggal"
        Session("last_cari_non_tanggal") = TextBox1.Text
        Session("last_cari_non_tanggal_kolom") = DropDownList1.Text

        Try
            'yourGrid.CurrentPageIndex = 0 '---> Untuk menghindari bug jika search dari page(pagination) terakhir
            'dt.DefaultView.RowFilter = state_dropdown + " like '%" + TextBox1.Text + "%'"
            dt.DefaultView.RowFilter = DropDownList1.SelectedItem.Value + " like '%" + TextBox1.Text + "%'"
            yourGrid.DataSource = dt.DefaultView
            yourGrid.DataBind()
            'TextBox1.Text = ""
            'TextBox1.Focus()

            txtDate.Text = ""

            'load_dropdownlist()
            'lbl_warning.Visible = False


                 For B As Integer = 0 To yourGrid.Items.Count - 1
                If yourGrid.Items(B).Cells.Item(8).Text = "dilayani" Then
                    yourGrid.Items(B).BackColor = Drawing.Color.Green
                Else
                    yourGrid.Items(B).BackColor = Drawing.Color.White
                End If
            Next



        Catch ex As Exception
            'lbl_warning.Visible = True
        End Try






    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Try
            Session("cari") = "tanggal"
            'yourGrid.CurrentPageIndex = 0 '---> Untuk menghindari bug jika search dari page(pagination) terakhir
            dt.DefaultView.RowFilter = "[TANGGAL] = '" + txtDate.Text + "'"

            yourGrid.DataSource = dt.DefaultView
            yourGrid.DataBind()
            'txtDate.Text = ""
            'txtDate.Focus()
            TextBox1.Text = ""

            'load_dropdownlist()
            'lbl_warning.Visible = False


            For B As Integer = 0 To yourGrid.Items.Count - 1
                If yourGrid.Items(B).Cells.Item(8).Text = "dilayani" Then
                    yourGrid.Items(B).BackColor = Drawing.Color.Green
                Else
                    yourGrid.Items(B).BackColor = Drawing.Color.White
                End If
            Next


        Catch ex As Exception

            Response.Write(ex)

            'lbl_warning.Visible = True
        End Try

    End Sub

    


    Protected Sub rb_dilayani_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_dilayani.Click
        Session("status") = "dilayani"
        Response.Redirect("Default.aspx?status=dilayani")
    End Sub

    Protected Sub rb_belum_dilayani_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_belum_dilayani.Click
        Session("status") = "belum_dilayani"
        Response.Redirect("Default.aspx?status=belum_dilayani")

    End Sub
End Class