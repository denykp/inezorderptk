﻿Public Class login1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        logout_check()
    End Sub

    Protected Sub btn_login_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_login.Click
        Try
            Login(Server.HtmlEncode(txt_username.Text), Server.HtmlEncode(txt_password.Text))
        Catch ex As Exception
        End Try
    End Sub


    Sub login(ByVal user As String, ByVal pass As String)
        Dim SqlDataSource1 As New SqlDataSource()
        SqlDataSource1.ID = "SqlDataSource1"
        Me.Page.Controls.Add(SqlDataSource1)
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("db_inezConnectionString").ConnectionString

        SqlDataSource1.SelectParameters.Clear()
        SqlDataSource1.SelectParameters.Add("user", user)
        SqlDataSource1.SelectParameters.Add("pass", pass)

        SqlDataSource1.SelectCommand = "select * from [user] where [email] = @user AND [password]= @pass"

        Dim dview As DataView = CType(SqlDataSource1.Select(DataSourceSelectArguments.Empty), DataView)
        If dview.Count > 0 Then

            Session("nama_user") = dview.Table.Rows(0)("nama_user")
            Session("role") = dview.Table.Rows(0)("role")

            Response.Redirect("Default.aspx")
            lbl_error.Visible = False
        Else
            lbl_error.Visible = True
        End If
    End Sub

    Sub logout_check()
        If Request.QueryString("logout") = "1" Then
            'Session.Contents.RemoveAll()
            Session.Clear()
            Session.Abandon()
            Response.Redirect("Default.aspx")
        End If
    End Sub

End Class